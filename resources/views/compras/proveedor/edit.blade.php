@extends ('layouts.admin')
@section ('contenido')
  <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h3>Editar Proveedor: {{$persona->nombre}}</h3>
        @if (count($errors)>0)
          <!--<div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
          </div>-->
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
          </ul>
        </div>
        @endif
      </div>
  </div>
        {!!Form::model($persona,['method'=>'PATCH','route'=>['proveedor.update',$persona->idpersona]])!!}
        {{Form::token()}}
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
              <label for="nombre">Nombre </label>
              <input type="text" name="nombre" required value="{{$persona->nombre}}" class="form-control">
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
              <label for="tipo_documento">Documento </label>
              <select class="form-control" name="tipo_documento">
                @if ($persona->tipo_documento=='DNI')
                  <option value="DNI" selected>DNI</option>
                  <option value="RUC">RUC</option>
                  <option value="PASAPORTE">PASAPORTE</option>
                @elseif ($persona->tipo_documento=='RUC')
                  <option value="DNI">DNI</option>
                  <option value="RUC" selected>RUC</option>
                  <option value="PASAPORTE">PASAPORTE</option>
                @else
                  <option value="DNI">DNI</option>
                  <option value="RUC">RUC</option>
                  <option value="PASAPORTE" selected>PASAPORTE</option>
                @endif
              </select>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
              <label for="num_documento">Número de Documento </label>
              <input type="text" name="num_documento" required value="{{$persona->num_documento}}" class="form-control">
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
              <label for="direccion">Dirección </label>
              <input type="text" name="direccion" value="{{$persona->direccion}}" class="form-control">
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
              <label for="telefono">Teléfono </label>
              <input type="text" name="telefono" value="{{$persona->telefono}}" class="form-control">
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
              <label for="email">Email </label>
              <input type="email" name="email" value="{{$persona->email}}" class="form-control">
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Guardar</button>
              <button type="button" onclick="history.back()" class="btn btn-danger">Cancelar</button>
            </div>
          </div>
        </div>
        {!!Form::close()!!}
@endsection
